import os, pexpect, inspect
class DEP_POS_GOV:
    prompt="--> "
    def __init__(self):
        cwd_orig=os.getcwd()
        self._process=pexpect.spawn(
            r"java -cp %s/target/my-app-1.0-SNAPSHOT.jar:%s/* %s" \
                %(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))),
                  os.path.expanduser(r"~/.m2/repository/edu/stanford/nlp/stanford-corenlp/3.3.0"), \
                  "edu.stanford.nlp.trees.GetGovernorDependencyPostag"))
        self._process.delaybeforesend=0
        assert self._process.isalive(), "Could not run command"
        self._process.expect_exact(self.prompt)
        assert self._process.after==self.prompt, "Is this corenlp supported ?"
        return
    
    def get_gov_dep_postags(self, tokens):
        """I accept tokens incstead of sentence because
        I do not performa any fancy tokenization in my postagger
        """
        assert type(tokens) is not str
        assert self._process.isalive(), "Liveliness check for postagger faile."
        self._process.sendline(" ".join([e.replace(" ", "").encode("ascii", "ignore") for e in tokens]))
        self._process.expect_exact(self.prompt, timeout=160)
        output=[e.strip().split("\t") for e in self._process.before.strip().split("\n")[1:]]
        try:
            assert len(output)==len(tokens)
        except:
            import pdb; pdb.set_trace()
        governors =[e[1] for e in output]
        deptypes  =[e[2] for e in output]
        newpostags=[e[3] for e in output]
        return [governors, deptypes, newpostags]






