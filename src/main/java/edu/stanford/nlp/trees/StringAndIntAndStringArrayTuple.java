package edu.stanford.nlp.trees;

public class StringAndIntAndStringArrayTuple {
	String[] depType;
	int[] gov;
	String[] postag;
	public StringAndIntAndStringArrayTuple(String[] depType, int[] gov,
			String[] postag) {
		this.depType=depType;
		this.gov=gov;
		this.postag=postag;
	}
	public String[] getDepType(){return depType;}
	public int[] getGov(){return gov;}
	public String[] getPos() {
		return postag;
	}
}