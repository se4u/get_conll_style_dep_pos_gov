package edu.stanford.nlp.trees;
import java.util.Scanner;

public class GetGovernorDependencyPostag {
	private static boolean printPromptAndReturnVal(String prompt, Scanner user){
		System.out.print(prompt);
		return user.hasNextLine();
	}

	public static void main(String[] args){
		GetGovAndDepType ggd = new GetGovAndDepType();
		Scanner user = new Scanner(System.in);
		while (printPromptAndReturnVal("--> ", user)) {
			String curline = user.nextLine().trim();
			if(curline.equals("")) continue;
			String[] arr = curline.split(" ");
			// Parse the array
			StringAndIntAndStringArrayTuple govndep = ggd.getGovAndDepTypeThroughParsing(arr);
			String[] depType = govndep.getDepType();
			int[] gov = govndep.getGov();
			String[] newPosTag = govndep.getPos();
			assert gov.length==depType.length;
			// Write to output
			for(int i=0; i<gov.length; i++){
				System.out.print(String.format("%s\t%d\t%s\t%s\n", arr[i], gov[i], depType[i], newPosTag[i]));
			}
		}
	}
}